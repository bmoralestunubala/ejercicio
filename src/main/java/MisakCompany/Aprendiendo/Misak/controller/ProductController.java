package MisakCompany.Aprendiendo.Misak.controller;

import MisakCompany.Aprendiendo.Misak.domain.dto.ProductDTO;
import MisakCompany.Aprendiendo.Misak.domain.dto.ProductForm;
import MisakCompany.Aprendiendo.Misak.domain.entity.Product;
import MisakCompany.Aprendiendo.Misak.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author danny
 * @project shopping
 * @class ProductController
 * @date 06/03/2021
 */
@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("products")
    public ResponseEntity<List<Product>> getAllProductsOrderByName() {
        return ResponseEntity.ok(productService.getAllProductsOrderByName());
    }

    @GetMapping("product/{id}")
    public ResponseEntity<Product> getById(@PathVariable Long id) {
        Product product = this.productService.getById(id);

        if (product.getId() != null)
            return ResponseEntity.ok(product);
        else
            return ResponseEntity.notFound().build();
    }

    @DeleteMapping("product/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        try {
            boolean isDeleted = this.productService.deleteById(id);

            if (isDeleted)
                return ResponseEntity.ok("Fue borrado con exito el producto " + id);
            else
                return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }

    }
    @PostMapping("product")
    public ResponseEntity<ProductDTO> saveProduct(@RequestBody ProductForm form) {
        try {
            ProductDTO product = this.productService.save(form);

            if (product.getId() != null)
                return ResponseEntity.ok(product);
            else
                return ResponseEntity.badRequest().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("products/{name}")
    public ResponseEntity<List<Product>> getAllByNameIsLike(@PathVariable String name) {
        return ResponseEntity.ok(productService.getAllByNameIsLike(name));
    }

    @GetMapping("product-search/{name}")
    public ResponseEntity<Product> getByName(@PathVariable String name) {
        Product product = this.productService.getByName(name);

        if (product.getId() != null)
            return ResponseEntity.ok(product);
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping("category/{categoryName}/product-sum")
    public ResponseEntity<Integer> getSumProductsByCategoryName(@PathVariable String categoryName) {
        Integer sum = this.productService.sumProductByCategoryName(categoryName);

        if (sum != -1)
            return ResponseEntity.ok(sum);
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping("category/{categoryName}/products")
    public ResponseEntity<List<Product>> getAllProductsByCategoryName(@PathVariable String categoryName) {
        return ResponseEntity.ok(productService.getAllProductsByCategoryName(categoryName));
    }
}
