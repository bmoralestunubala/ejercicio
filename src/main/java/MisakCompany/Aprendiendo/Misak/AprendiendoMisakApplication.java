package MisakCompany.Aprendiendo.Misak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AprendiendoMisakApplication {

	public static void main(String[] args) {
		SpringApplication.run(AprendiendoMisakApplication.class, args);

	}

}
