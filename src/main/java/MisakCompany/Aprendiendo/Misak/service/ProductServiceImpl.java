package MisakCompany.Aprendiendo.Misak.service;

import MisakCompany.Aprendiendo.Misak.domain.dto.CategoryDTO;
import MisakCompany.Aprendiendo.Misak.domain.dto.ProductDTO;
import MisakCompany.Aprendiendo.Misak.domain.dto.ProductForm;
import MisakCompany.Aprendiendo.Misak.domain.entity.Category;
import MisakCompany.Aprendiendo.Misak.domain.entity.Product;
import MisakCompany.Aprendiendo.Misak.domain.entity.ProductCategory;
import MisakCompany.Aprendiendo.Misak.domain.repository.CategoryRepository;
import MisakCompany.Aprendiendo.Misak.domain.repository.ProductCategoryRepository;
import MisakCompany.Aprendiendo.Misak.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author danny
 * @project shopping
 * @class ProductServiceImpl
 * @date 06/03/2021
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Override
    public List<Product> getAllProductsOrderByName() {
        return productRepository.findAllByOrderByName();
    }

    @Override
    public List<Product> getAllByNameIsLike(String name) {
//        return productRepository.findByNameContains(name);
        return productRepository.getAllByNameIsLike("%".concat(name).concat("%"));
    }

    @Override
    public Product getById(Long id) {
        return this.productRepository.findById(id).orElse(new Product());
    }

    @Override
    public boolean deleteById(Long id) {
        // 1. que exista el producto
        Product product = productRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("no existe el id del producto"));
        List<ProductCategory> productCategoryList = productCategoryRepository.findAllByIdProduct(product.getId());
        productCategoryRepository.deleteAll(productCategoryList);
        productRepository.deleteById(product.getId());

        return true;
    }

    @Override
    public Product getByName(String name) {
        return this.productRepository.findByName(name).orElse(new Product());
    }

    @Override
    public List<Product> getAllProductsByCategoryName(String categoryName) {
        return this.productRepository.getAllByCategoryNameNative(categoryName);
    }

    @Override
    public Integer sumProductByCategoryName(String categoryName) {
        return this.productRepository.sumProductByCategoryName(categoryName).orElse(-1);
    }

    @Override
    public ProductDTO save(ProductForm form) {
        Product product = new Product();
        // 1. guardar producto
        product.setName(form.getName());
        product.setDescription(form.getDescription());
        product.setPrice(form.getPrice());
        product = productRepository.save(product);

        // 2. validar categoria exista
        Category category = categoryRepository.findById(form.getIdCategory())
                .orElseThrow(() -> new IllegalArgumentException("no existe el id cateogria"));

        // 3. guardar en tabla de relacion id categoria e id producto
        ProductCategory productCategory = new ProductCategory();
        productCategory.setIdCategory(category.getId());
        productCategory.setIdProduct(product.getId());
        productCategoryRepository.save(productCategory);

        ProductDTO responseDTO = new ProductDTO();

        responseDTO.setId(product.getId());
        responseDTO.setName(product.getName());
        responseDTO.setDescription(product.getDescription());
        responseDTO.setPrice(product.getPrice());

        CategoryDTO categoryDTO = new CategoryDTO();

        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        categoryDTO.setDescription(category.getDescription());
        responseDTO.setCategory(categoryDTO);
        return responseDTO;
    }
}
