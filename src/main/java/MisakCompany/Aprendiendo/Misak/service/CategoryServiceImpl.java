package MisakCompany.Aprendiendo.Misak.service;

import MisakCompany.Aprendiendo.Misak.domain.entity.Category;
import MisakCompany.Aprendiendo.Misak.domain.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author danny
 * @project shopping
 * @class CategoryServiceImpl
 * @date 05/03/2021
 */
@Service // lo expone como un servicio
@Transactional // punto a punto (commit) sino hace commit -> rollback
public class CategoryServiceImpl implements CategoryService {

    @Autowired // inyección de dependencias
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public Category save(Category category) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }
}
