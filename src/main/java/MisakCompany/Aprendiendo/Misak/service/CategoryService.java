package MisakCompany.Aprendiendo.Misak.service;

import MisakCompany.Aprendiendo.Misak.domain.entity.Category;

import java.util.List;
import java.util.Optional;

/**
 * @author danny
 * @project shopping
 * @class CategoryService
 * @date 05/03/2021
 * Patron Facade
 */
public interface CategoryService {
    /**
     * CAPA DE LOGICA DE NEGOCIO - PROGRAMACIÓN - SERVICIOS (microservicios)
     */

    List<Category> findAll();

    Optional<Category> findById(Long id);

    Category save(Category category);

    void deleteById(Long id);
}
