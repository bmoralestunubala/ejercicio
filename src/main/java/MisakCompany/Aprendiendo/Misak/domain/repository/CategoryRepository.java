package MisakCompany.Aprendiendo.Misak.domain.repository;

import MisakCompany.Aprendiendo.Misak.domain.entity.Category;
import MisakCompany.Aprendiendo.Misak.domain.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    /**
     *CAPA DE ACCESO A DATOS -  REPOSITORIO DE CONSULTAS - DAO (Data Acces Object)
     */

    //CRUD
    @Override
    List<Category> findAll();

    Optional<Category> findById(Long id);

    Category save(Category category);

    void  deleteById(Long id);

    @Query(value = "select pd from Product pd where  pd.name   =:name")
    List<Category>getAllByName2(@Param("name")String name);

}