package MisakCompany.Aprendiendo.Misak.domain.repository;


import MisakCompany.Aprendiendo.Misak.domain.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {

    List<ProductCategory> findAllByIdProduct(Long idProduct);
}
